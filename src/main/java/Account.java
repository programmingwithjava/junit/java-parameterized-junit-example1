public class Account {

    private String name;
    private String surname;
    private double balance;

    public static final int CHECKING = 1;
    public static final int SAVINGS = 2;

    private int accountType;

    public Account(String name, String surname, double balance, int accountType) {
        this.name = name;
        this.surname = surname;
        this.balance = balance;
        this.accountType = accountType;
    }

    /*
    The branch argument is true if the customer is performing the transaction
    at a branch, with a teller.
    It's false if the customer is performing the transaction at an ATM.
    * */
    public double deposit(double amount, boolean branch) {
        balance += amount;
        return balance;
    }

    public double withdraw(double amount, boolean branch) {
        /*
        * you cant withdraw more than 500 from an ATM
        * */
        if(amount > 500 && !branch){
            throw new IllegalArgumentException();
        }
        balance -= amount;
        return balance;
    }

    public double getBalance() {
        return balance;
    }
    public boolean isChecking(){
        return accountType == CHECKING;
    }
}
