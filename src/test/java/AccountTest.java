import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/*
 * tell JUnit that this is not a normal test class.
 * we tell JUnit test we will be running parameterized tests in this class.
 * so we use @RunWith annotation.
 * 
 * we need to create a class for each parameterized test we wanna run. unless all the tests are using the same parameters.
 * */
@RunWith(Parameterized.class)
public class AccountTest {

    private Account account;
    
    private double amount;
    private boolean branch;
    private double expected;
    
    public AccountTest(double amount, boolean branch, double expected) {
		this.amount = amount;
		this.branch = branch;
		this.expected = expected;
	}

	/*
     * now we will tell JUnit that these are the parameters that we wanna use.
     * we will add a "STATIC" method annotated with "@Parameters" annotation. this method should return a "collection".
     * each row is a set of parameters that we want to test followed by the expected value.
     * when we run the test, JUnit will create an instance of the "AccountTest" class for 
     * each set of test data using Test Class's constructor above.
     * It will use the test class's constructor to set the instance variables to the set of values we specify below. 
     * So we add the instance variables that we want to set/change and we add a constructor that accepts those parameters.
     * */
    @Parameterized.Parameters
    public static Collection<Object[]> testConditions(){
    	return Arrays.asList(new Object[][] {
    		{100.00, true, 1100.00}, 
    		{200.00, true, 1200.00},
    		{150.00, true, 1150.00}
    	});
    } 
    /*
     * be careful. we are still keeping the @Before method that instantiates the Account class for us before each test.
     * parameters are used inside the test method to change some conditions.
     * */
    @Before
    public void beforeTest() {
        account = new Account("serdar", "onur", 1000.00, Account.CHECKING);
        System.out.println("running test method...");
    }
    
    @org.junit.Test
    public void deposit() throws Exception {
        account.deposit(amount, branch);
        assertEquals(expected, account.getBalance(), .01);
    }
}